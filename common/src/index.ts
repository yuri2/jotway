export * from './types/entities';
export * from './types/serverData';
export * from './types/bookmarks';
export * from './utils/app';
export const appVersion = '1.2.1';
export const githubHref = 'https://github.com/yuri2peter/jotway';
